#define SS_PIN 2  //D2
#define RST_PIN 0 //D1

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <SPI.h>
#include <MFRC522.h>
#include <ArduinoJson.h>

String kirim;

const char* ssid = "jepri"; //jangan lupa diisi sama ssid
const char* password = "qwerty123"; //jangan lupa diisi sama password


MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
int statuss = 0;
int out = 0;
void setup() 
{
  Serial.begin(115200);    // Initialize serial communications with the PC
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
    WiFi.persistent(false); 
  delay(5000);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Waiting for connection");
    Serial.write("Waiting for connection");
  }
  delay (5000);
}
void loop() 
{
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }

      //Show UID on serial monitor
  Serial.println();
  Serial.print(" UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
     //json'
     StaticJsonDocument<256> doc;
     JsonObject root = doc.to<JsonObject>();
      root["id"] = content;
      root["saldo"] = 5000;
      serializeJson(root, kirim);
     serializeJson(root, Serial);
  
  delay (5000);  
  
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.begin("http://stormy-eyrie-51990.herokuapp.com/kurangsaldo"); //jangan lupa diganti sama server punya kalian. //coba.php itu cuma buat ngehandle data yang masuk, terus dimasukin ke database
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    Serial.print("Berhasil konek wifi");
    
    int httpCode = http.POST(kirim); //ngirim data
    delay(7000);
    String payload = http.getString();

    Serial.println(httpCode);
    Serial.println(payload);

    http.end();
  } else {
    Serial.println("Error connect.");
  }
  
} 
